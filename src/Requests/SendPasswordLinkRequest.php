<?php

namespace WeSimplyCode\ApiPasswordReset\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;

class SendPasswordResetLinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:60'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        new ValidationException($validator);
        throw new HttpResponseException(response(['message' => $validator->errors()->all()], 422));
    }
}