<?php

namespace WeSimplyCode\ApiPasswordReset\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

trait PasswordResetToken
{
    protected function createToken($email)
    {
        $oldToken = DB::table('password_reset')->whereEmail($email)->first();

        if ($oldToken) {
            $this->deleteToken($email);
        }

        $token = str_random(60);
        $this->saveToken($token, $email);

        return $token;
    }

    protected function saveToken($token, $email)
    {
        DB::table('password_reset')->insert([
            'email' => $email,
            'token' => $token,
            'created_at' => Carbon::now(),
            'expires_at' => Carbon::now()->addDays(config('passwordReset.token_expiration'))
        ]);
    }

    public function checkToken($token, $email)
    {
        $passwordReset = DB::table('password_reset')->select('token', 'expires_at')->whereEmail($email)->first();

        if (!$passwordReset) {
            return false;
        } elseif ($token != $passwordReset->token) {
            return false;
        } elseif (Carbon::now()->gt($passwordReset->expires_at)) {
            return false;
        } else {
            return true;
        }
    }

    protected function deleteToken($email)
    {
        DB::table('password_reset')->whereEmail($email)->delete();
    }
}