<?php

namespace WeSimplyCode\ApiPasswordReset;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Collection;

class ApiPasswordResetServiceProvider extends ServiceProvider
{
    public function boot(Filesystem $filesystem)
    {
        // include __DIR__ . '/routes.php';
        $this->loadRoutesFrom(__DIR__ . '/routes.php');

        $this->publishes([
            __DIR__ . '/../config/passwordReset.php' => config_path('passwordReset.php'),
        ], 'wesimplycode-passwordreset-config');

        $this->publishes([
            __DIR__ . '/../migrations/create_password_reset_table.php' => $this->getMigrationFileName($filesystem),
        ], 'wesimplycode-passwordreset-migrations');

        $this->publishes([
            __DIR__ . '/Views/emails/passwordReset.blade.php' => resource_path('views/emails/passwordReset.blade.php'),
        ], 'wesimplycode-passwordreset-views');
    }

    public function register()
    {
        $this->app->make('WeSimplyCode\ApiPasswordReset\Controllers\PasswordForgotController');
        $this->app->make('WeSimplyCode\ApiPasswordReset\Controllers\PasswordResetController');
    }

    /**
     * Returns existing migration file if found, else uses the current timestamp.
     *
     * @param Filesystem $filesystem
     * @return string
     */
    protected function getMigrationFileName(Filesystem $filesystem): string
    {
        $timestamp = date('Y_m_d_His');
        return Collection::make($this->app->databasePath() . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem) {
                return $filesystem->glob($path . '*_create_password_reset_table.php');
            })->push($this->app->databasePath() . "/migrations/{$timestamp}_create_password_reset_table.php")
            ->first();
    }
}