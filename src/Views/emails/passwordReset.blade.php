@component('mail::message')
Hi {{$user->name}},

Someone has requested a password reset.<br>

{{$host.'/?token='.$token}}
@endcomponent
