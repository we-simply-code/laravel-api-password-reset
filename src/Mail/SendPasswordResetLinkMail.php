<?php

namespace WeSimplyCode\ApiPasswordReset\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class SendPasswordResetLinkMail extends Mailable
{
    use SerializesModels;

    public $user;
    public $token;
    public $host;

    public function __construct(User $user, $token, $host)
    {
        $this->user = $user;
        $this->token = $token;
        $this->host = $host;
    }

    public function build()
    {
        return $this->subject('Reset password')->markdown('emails.passwordReset');
    }
}