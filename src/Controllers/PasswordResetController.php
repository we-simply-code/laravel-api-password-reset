<?php

namespace WeSimplyCode\ApiPasswordReset\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use WeSimplyCode\ApiPasswordReset\Traits\PasswordResetToken;
use WeSimplyCode\ApiPasswordReset\Requests\ResetPasswordRequest;

class PasswordResetController extends Controller
{
    use PasswordResetToken;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        $user = User::whereEmail($request->email)->first();

        if ($user == null) {
            return response(['message' => 'Email address not found!'], 404);
        }

        if (!$this->checkToken($request->token, $request->email)) {
            return response()->json(['message' => 'Token expired!'], 498);
        }

        $user->password = Hash::make($request->password);
        $user->save();

        $this->deleteToken($user->email);

        return response(['message' => 'Password changed successfully!'], 200);
    }
}