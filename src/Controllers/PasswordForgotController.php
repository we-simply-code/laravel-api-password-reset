<?php

namespace WeSimplyCode\ApiPasswordReset\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use WeSimplyCode\ApiPasswordReset\Traits\PasswordResetToken;
use WeSimplyCode\ApiPasswordReset\Mail\SendPasswordResetLinkMail;
use WeSimplyCode\ApiPasswordReset\Requests\SendPasswordResetLinkRequest;

class PasswordForgotController extends Controller
{
    use PasswordResetToken;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendPasswordResetLink(SendPasswordResetLinkRequest $request)
    {
        if (!$this->validateEmail($request->email)) {
            return response(['message' => 'Reset code could not be send!'], 404);
        }

        $token = $this->createToken($request->email);
        $user = User::whereEmail($request->email)->first();
        $host = config('passwordReset.host');

        Mail::to($user->email)->send(new SendPasswordResetLinkMail($user, $token, $host));

        return response(['message' => 'Reset code was sent!'], 200);
    }

    protected function validateEmail($email)
    {
        return !!User::whereEmail($email)->first();
    }
}