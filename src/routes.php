<?php

Route::group(['namespace' => 'WeSimplyCode\ApiPasswordReset\Controllers'], function () {
    Route::post('/api/password/forgot', 'PasswordForgotController@sendPasswordResetLink')->name('password.forgot');
    Route::post('/api/password/reset', 'PasswordResetController@resetPassword')->name('password.reset');
});