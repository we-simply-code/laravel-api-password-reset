# API-Password-Reset

Simple password reset package for api's build with laravel framework.

### Installing

```
composer require wesimplycode/api-password-reset
```

```
php artisan vendor:publish --provider="WeSimplyCode\ApiPasswordReset\ApiPasswordResetServiceProvider" --tag="wesimplycode-passwordreset-config"
```

```
php artisan vendor:publish --provider="WeSimplyCode\ApiPasswordReset\ApiPasswordResetServiceProvider" --tag="wesimplycode-passwordreset-migrations"
```

```
php artisan vendor:publish --provider="WeSimplyCode\ApiPasswordReset\ApiPasswordResetServiceProvider" --tag="wesimplycode-passwordreset-views"
```

When using Laravel 5.5 or higher the service provider will automatically get registered.

After publishing the migrations you can create all the tables needed by running

```
php artisan migrate
```

### Usage

This package creates two routes.

```php
Route::group(['namespace' => 'WeSimplyCode\ApiPasswordReset\Controllers'], function () {
    Route::post('/api/password/forgot', 'PasswordForgotController@sendPasswordResetLink')->name('password.forgot');
    Route::post('/api/password/reset', 'PasswordResetController@resetPassword')->name('password.reset');
});
```

To use this package simply make the user enter their email adress and send it to the route "password.forgot". This will check if the email exists for a user and than generate a token which will be mailed to the user email adress.

To reset the password you can make the user enter the token they received in their email allong side their email adress and new password (must be confirmed) and send that data to the route "password.reset".

**This package doesn't come with the forms to reset the password. You can make those forms as long as you point them to the correct routes!**

You can adjust the email that is being send to the user by adjusting the email template in "resources/views/emails/passwordReset.blade.php".

You can change the host url and the time for the token to expire in the config. The config is in "config/passwordReset.php". The time for the token to expire is in days.

## Author

- **Sunil Kisoensingh**

## License

This project is licensed under the MIT License
